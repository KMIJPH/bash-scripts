#!/usr/bin/env bash
# File Name: usb.sh
# Description: Usb module
# Dependencies: awk, bemenu
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 16 Aug 2023 14:27:54
# Last Modified: 05 Sep 2023 21:33:15

set -eufo pipefail
IFS=$'\n'

COL_BG="#1c1c1c"
COL_GREY0="#2e3440"
COL_GREEN0="#a3be8c"

bm() {
    bemenu \
        -n -i -H 35 -p "󰕓" \
        --fn "SauceCodePro NFM Regular 11" \
        --tb "$COL_BG" --nb "$COL_BG" --ab "$COL_BG" --fb "$COL_BG" --cb "$COL_BG" --sb "$COL_GREY0" \
        --hf "$COL_GREEN0" --tf "$COL_GREEN0" --cf "$COL_BG"
}

list_usbs() {
    mount | grep "/run/media" | awk '{print $1}'
}

unmount() {
    local mounts
    readarray -t mounts <<<"$(list_usbs)"

    local names
    readarray -t names <<<"$(mount | grep "/run/media" | awk '{print $3}')"

    local choice
    choice="$(printf "%s\n" "${names[@]}" | bm)"

    if [[ -n "$choice" ]]; then
        local counter=0
        for name in "${names[@]}"; do
            if [[ "$name" == "$choice" ]]; then
                umount "${mounts[$counter]}"
            fi
            counter=$((counter + 1))
        done
    fi
}

to_waybar() {
    local usbs
    usbs=$(list_usbs | wc -l || true)

    if [[ "$usbs" -gt 0 ]]; then
        printf '{"text": %2d, "tooltip": "%d devices connected", "class": "module", "percentage": 100}' "$usbs" "$usbs"
    else
        printf '{"text": %2d, "tooltip": "no devices connected", "class": "module", "percentage": 0}' "$usbs"
    fi
}

exec_if() {
    local usbs
    usbs=$(list_usbs | wc -l || true)

    if [[ "$usbs" -gt 0 ]]; then
        exit 0
    else
        exit 1
    fi
}

# prints help message
print_help() {
    local help=(
        "usage: bar-usb"
        "  count|count mounted usb devices and return a json message"
        "  unmount|open a bemenu to unmount a device"
        "  exec-if|check if a device is mounted"
    )

    printf "%s\n" "${help[@]}" | column -t -s '|' >&2
    exit 1
}

if [[ "$#" -eq 0 ]]; then
    print_help
else
    case "$1" in
    count)
        to_waybar
        ;;
    unmount)
        unmount
        ;;
    exec-if)
        exec_if
        ;;
    *)
        print_help
        ;;
    esac
fi
