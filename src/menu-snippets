#!/usr/bin/env bash
# File Name: menu-snippets
# Description: Simple snippets until helix has custom snippets
# Author: KMIJPH
# Repository: https://codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 13 Jan 2025 21:31:08
# Last Modified: 13 Jan 2025 21:31:08

COL_BG="#1c1c1c"
COL_GREY0="#2e3440"
COL_RED="#bf616a"
SNIPPET_DIR="$HOME/.config/helix/snippets"
FILES=()
KEYS=()

bm() {
	bemenu \
		-n -i -H 35 -p "⏻" \
		--fn "SauceCodePro NFM Regular 11" \
		--tb "$COL_BG" --nb "$COL_BG" --ab "$COL_BG" --fb "$COL_BG" --cb "$COL_BG" --sb "$COL_GREY0" \
		--hf "$COL_RED" --tf "$COL_RED" --cf "$COL_BG"
}

find_files() {
	while IFS= read -r file; do
		FILES+=("$file")
	done < <(find "$SNIPPET_DIR" -type f -name "*.json")
}

read_keys() {
	for file in "${FILES[@]}"; do
		if [[ -f "$file" ]]; then
			local keys=("$(jq -r 'keys[]' "$file")")
			KEYS+=("${keys[@]}")
		else
			echo "File $file does not exist"
		fi
	done
}

get_body() {
	local key="$1"
	local body=""
	local result=""

	for file in "${FILES[@]}"; do
		body=$(jq --arg k "$key" -r '.[$k].body // empty | join("\n")' "$file")

		if [[ -n "$body" ]]; then
			result+="$body"$'\n'
		fi
	done

	if [[ -n "$result" ]]; then
		echo -n "$result"
		return 0
	fi

	echo "Key '$key' not found in any file." >&2
	return 1
}

list_keys() {
	for key in "${KEYS[@]}"; do
		echo "$key"
	done
}

menu() {
	find_files
	read_keys
	local choice
	choice=$(list_keys | bm)

	if [[ -n "$choice" ]]; then
		local body
		body=$(get_body "$choice")

		if [[ -n "$body" ]]; then
			printf "%s" "$body" | wl-copy
		fi
	fi
}

menu
