Some bash scripts.

## arkive
Convenience script for extracting archive formats.

## bib
Simple bibliography manager that allows to fetch bibtex entries via DOI.  
Can also be used to download articles (if a download provider is passed).

## bm
Simple bookmark manager.

## lc
Counts lines of code in a directory.

## lf-actions
Common actions for the [lf filemanager](https://github.com/gokcehan/lf).

## todo
Simple todo and calendar thingy built around [remind](https://dianne.skoll.ca/projects/remind/).

## wettr
Weather forecasts etc. from [OWM](https://openweathermap.org/).

## menu-*
A bunch of menu scripts for [bemenu](https://github.com/Cloudef/bemenu).

## bar-*
A bunch of [waybar](https://github.com/Alexays/Waybar) scripts.
